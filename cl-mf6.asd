;;;; cl-mf6.asd

(asdf:defsystem #:cl-mf6
  :description "Modflow 6 wrapper"
  :author "William Witterick"
  :license  "GPL v3"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "cl-mf6")))
