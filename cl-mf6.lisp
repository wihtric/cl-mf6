;;; cl-mf6.lisp ---
;;
;; Filename: cl-mf6.lisp
;; Description:
;; Author: William Witterick
;; Maintainer:
;; Created: Sat Mar 20 18:14:46 2021 (+0000)
;; Version:
;; Last-Updated:
;;           By:
;;     Update #: 0
;; URL:
;; Keywords:
;; Compatibility:
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Change Log:
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(in-package #:cl-mf6)

(defparameter *str-foreign-type* '())
(setq *str-foreign-type* (make-hash-table :test 'equal))
(setf (gethash "double" *str-foreign-type*) :double)
(setf (gethash "float" *str-foreign-type*) :float)
(setf (gethash "int32" *str-foreign-type*) :int32)

(defparameter *str-lisp-type* '())
(setq *str-lisp-type* (make-hash-table :test 'equal))
(setf (gethash "double" *str-lisp-type*) 'double-float)
(setf (gethash "float" *str-lisp-type*) 'single-float)
(setf (gethash "int32" *str-lisp-type*) '(signed-byte 32))

(cffi:define-foreign-library lib-mf6
    (t (:default "libmf6"))) ;; note no .so suffix here

(cffi:use-foreign-library lib-mf6)

(cffi:defcvar ("ISTDOUTTOFILE" *istdouttofile*) :int)

(setf *istdouttofile* 0)

(cffi:defcvar ("BMI_LENVARADDRESS" *len-var-address*) :int)

(defcfun ("get_var_address" c-get-var-address)
    (:pointer :int)
  (component-name :pointer :char)
  (subcomponent-name :pointer :char)
  (var-name :pointer :char)
  (var-address :pointer :char :count *len-var-address*))

(defun get-var-address (var-name component-name &optional (subcomponent-name ""))
  (with-foreign-strings ((ccomp-name component-name)
			 (csub-name subcomponent-name)
			 (cvar-name var-name))
    (with-foreign-object (cvar-address :char)
      (loop for i from 0 below *len-var-address*
	    do
	       (setf (mem-aref cvar-address :char i) 0))
      (values
       (c-get-var-address ccomp-name csub-name cvar-name cvar-address))
      (coerce
       (loop for i from 0 below *len-var-address*
	     if (> (mem-aref cvar-address :char i) 0)
	       collect (code-char (mem-aref cvar-address :char i)))
       'string))))

(cffi:defcvar ("BMI_LENVARTYPE" *len-var-type*) :int)

(defcfun ("get_var_type" c-get-var-type)
    (:pointer :int)
  (var-address :pointer :char)
  (var-type :pointer :char :count *len-var-type*))

(defun get-var-type (var-address)
  (with-foreign-strings ((cvar-address var-address))
    (with-foreign-object (cvar-type :pointer)
      (loop for i from 0 below *len-var-type*
	  do
	     (setf (mem-aref cvar-type :char i) 0))
      (values
       (c-get-var-type cvar-address cvar-type))
      (coerce
       (loop for i from 0 below *len-var-type*
	     if (> (mem-aref cvar-type :char i) 0)
	       collect (code-char (mem-aref cvar-type :char i)) )
       'string))))

(defcfun ("get_var_itemsize" c-get-var-itemsize)
    (:int32)
  (var-address :pointer :char)
  (var-size :pointer :int32))

(defun get-var-itemsize (var-address)
  (with-foreign-string (cvar-address var-address)
    (with-foreign-object (cvar-size :int)
      (c-get-var-itemsize cvar-address cvar-size)
      (mem-aref cvar-size :int32))))

(defcfun ("get_var_rank" c-get-var-rank)
    (:int32)
  (var-address :pointer :char)
  (var-rank :pointer :int32))

(defun get-var-rank (var-address)
  (with-foreign-string (cvar-address var-address)
    (with-foreign-object (cvar-rank :int)
      (c-get-var-rank cvar-address cvar-rank)
      (mem-aref cvar-rank :int32))))

(defcfun ("get_var_nbytes" c-get-var-nbytes)
    (:int32)
  (var-address :pointer :char)
  (var-nbytes :pointer :int32))

(defun get-var-nbytes (var-address)
  (with-foreign-string (cvar-address var-address)
    (with-foreign-object (cvar-nbytes :int)
      (c-get-var-nbytes cvar-address cvar-nbytes)
      (mem-aref cvar-nbytes :int32))))

(defcfun ("get_value_ptr_int" c-get-value-ptr-int32)
    (:int32)
  (var-address :pointer :char)
  (value-ptr-int :pointer :int64))

(defcfun ("get_value_ptr_double" c-get-value-ptr-double)
    (:int32)
  (var-address :pointer :char)
  (value-ptr-int :pointer :int64))

(defcfun ("get_value_ptr_float" c-get-value-ptr-float)
    (:int32)
  (var-address :pointer :char)
  (value-ptr-int :pointer :int64))

(defcfun ("get_var_shape" c-get-var-shape)
    (:int32)
  (var-address :pointer :char)
  (var-shape :pointer :int32))

(defun get-var-shape (var-address)
  (with-foreign-string (cvar-address var-address)
    (let ((rank (get-var-rank var-address)))
      (with-foreign-object (cvar-shape :int rank)
	(c-get-var-shape cvar-address cvar-shape)
	(loop for i from 0 below rank
	   collect (mem-aref cvar-shape :int32 i))))))

(defmacro defun-get-value (fun-name return-type)
  (let ((fn (string-upcase (concatenate 'string "c-get-value-ptr-" return-type))))
    `(defun ,fun-name (name)
       (with-foreign-string (cname name)
	 (with-foreign-object (cvalue-ptr :int64)
	   (funcall (intern ,fn) cname cvalue-ptr)
	   (let ((f-ptr (mem-aref cvalue-ptr :int64)) ; int value of fortran pointer
		 (rank (get-var-rank name))
		 (shape (get-var-shape name)))
	     (cond ((= rank 0)
		    (mem-aref (make-pointer f-ptr) (gethash ,return-type *str-foreign-type*)))
		   ((= rank 1)
		    (let
		      ((farray0 (apply #'make-array
				    (concatenate 'list shape
						 (list :element-type
						       (gethash ,return-type
								*str-lisp-type*))))))

		      (loop for j from 0 below (car shape)
			    do (setf  (aref farray0 j)
				      (mem-aref (make-pointer f-ptr)
						(gethash ,return-type *str-foreign-type*) j)))
		      farray0)))))))))

(defun-get-value get-value-double "double")
(defun-get-value get-value-int "int32")
(defun-get-value get-value-float "float")

(defun get-value (name)
  (let ((vartype (string-downcase (get-var-type name))))
    (cond ((search "double" vartype)
	   (get-value-double name))
	  ((search "float" vartype)
	   (get-value-float name))
	  ((search "int" vartype)
	   (get-value-int name))
	   )))


(defmacro defun-set-value (fun-name return-type)
  (let ((fn (string-upcase (concatenate 'string "c-get-value-ptr-" return-type))))
    `(defun ,fun-name (name new-val)
       (with-foreign-string (cname name)
	 (with-foreign-object (cvalue-ptr :int64)
	   (funcall (intern ,fn) cname cvalue-ptr)
	   (let ((f-ptr (mem-aref cvalue-ptr :int64)) ; int value of fortran pointer
		 (rank (get-var-rank name))
		 (shape (get-var-shape name)))
	     (cond ((= rank 0)
		    (setf (mem-aref (make-pointer f-ptr)
				    (gethash ,return-type *str-foreign-type*)) new-val))
		   ((= rank 1)
		    (loop for j from 0 below (car shape)
			  do (setf (mem-aref (make-pointer f-ptr)
					     (gethash ,return-type *str-foreign-type*) j)
				   (aref new-val j)))))))))))


(defun-set-value set-value-double "double")
(defun-set-value set-value-int "int32")
(defun-set-value set-value-float "float")

(defun set-value (name new-val)
  (let ((vartype (string-downcase (get-var-type name))))
    (cond ((search "double" vartype)
	   (set-value-double name new-val))
	  ((search "float" vartype)
	   (set-value-float name new-val))
	  ((search "int" vartype)
	   (set-value-int name new-val)))))

(defsetf get-value-double set-value-double)
(defsetf get-value-float set-value-float)
(defsetf get-value-int set-value-int)
;;;  (defsetf get-value set-value)

(defcfun ("initialize" initialise)
    (:void))

(defcfun ("finalize" finalise)
    (:void))

(defcfun ("update" update)
    (:void))

(defcfun ("get_current_time" c-get-current-time)
    (:int32)
  (var-time :pointer :int32))

(defun get-current-time ()
  (with-foreign-object (cvar-time :int)
    (c-get-current-time cvar-time)
    (mem-aref cvar-time :double)))

(defcfun ("get_start_time" c-get-start-time)
    (:int32)
  (var-time :pointer :int32))

(defun get-start-time ()
  (with-foreign-object (cvar-time :int)
    (c-get-start-time cvar-time)
    (mem-aref cvar-time :double)))

(defcfun ("get_end_time" c-get-end-time)
    (:int32)
  (var-time :pointer :int32))

(defun get-end-time ()
  (with-foreign-object (cvar-time :int)
    (c-get-end-time cvar-time)
    (mem-aref cvar-time :double)))

(defcfun ("get_time_step" c-get-time-step)
    (:int32)
  (var-time :pointer :double))

(defun get-time-step ()
  (with-foreign-object (cvar-time :int)
    (c-get-time-step cvar-time)
    (mem-aref cvar-time :double)))

(defcfun ("prepare_time_step" c-prepare-time-step)
    (:void)
  (var-time :pointer :double))

(defun prepare-time-step (var-time)
  (let ((cvar-time (foreign-alloc :double :initial-element var-time)))
    (c-prepare-time-step cvar-time)
    (foreign-free cvar-time)))

(defcfun ("get_subcomponent_count" c-get-subcomponent-count)
    (:int32)
  (subcomponent-count :pointer :int))

(defun get-subcomponent-count ()
  (with-foreign-object (subcomponent-count :pointer)
    (c-get-subcomponent-count subcomponent-count)
    (mem-aref subcomponent-count :int)))

(defcfun ("prepare_solve" c-prepare-solve)
    (:void)
  (subcomponent-id :pointer :int))

(defun prepare-solve (&optional (subcomponent-id 1))
  (let ((c-subcomponent-id (foreign-alloc :int :initial-element subcomponent-id)))
    (c-prepare-solve c-subcomponent-id)
    (foreign-free c-subcomponent-id)))

(defcfun ("solve" c-solve)
    (:int32)
  (subcomponent-id :pointer :int)
  (has-converged :pointer :int))

(defun do-solve (&optional (subcomponent-id 1))
  (let ((c-subcomponent-id
	  (foreign-alloc :int :initial-element subcomponent-id)))
    (with-foreign-object (has-converged :pointer)
      (c-solve c-subcomponent-id has-converged)
      (foreign-free c-subcomponent-id)
      (if (= 0 (mem-aref has-converged :int))
	  nil
	  1))))

(defcfun ("finalize_solve" c-finalise-solve)
    (:void)
  (subcomponent-id :pointer :int))

(defun finalise-solve (&optional (subcomponent-id 1))
  (let ((c-subcomponent-id
	  (foreign-alloc :int :initial-element subcomponent-id)))
    (c-finalise-solve c-subcomponent-id)
    (foreign-free c-subcomponent-id)))

(defcfun ("finalize_time_step" finalise-time-step)
    (:void))

(defcfun ("get_input_item_count" c-get-input-item-count)
    (:void)
  (ccount :pointer :int32))

(defun get-input-item-count ()
  (with-foreign-object (ccount :pointer)
    (c-get-input-item-count ccount)
    (mem-aref ccount :int32)))

(defvar *len-names*)
(setf *len-names* (* *len-var-address* (get-input-item-count)))

(defcfun ("get_input_var_names" c-get-input-var-names)
    (:int)
  (names :pointer :char :count *len-names*))

(defun get-input-var-names ()
  (with-foreign-pointer (cnames *len-names*)
    (loop for i from 0 below *len-names*
	  do
	     (setf (mem-aref cnames :char i) 0))
    (c-get-input-var-names cnames)
    (let ((big-str
	    (coerce
	     (loop for i from 0 below *len-names*
		   collect
		   (code-char (mem-aref cnames :char i)))
	     'string)))
      (loop for i from 0 below (get-input-item-count)
	    collect (first (cl-ppcre:split (code-char 0)
					   (subseq big-str (* *len-var-address* i)
						   (* (+ 1 i) *len-var-address*))))))))


(defmacro zap (fn place &rest args)
  (multiple-value-bind
        (temps exprs stores store-expr access-expr)
      (get-setf-expansion place)
    `(let* (,@(mapcar #'list temps exprs)
            (,(car stores)
              (funcall ,fn ,access-expr ,@args)))
       ,store-expr)))


(defun array-map (function array
                  &optional (retval (make-array (array-dimensions array))))
  "Apply FUNCTION to each element of ARRAY.
Return a new array, or write into the optional 3rd argument."
  (dotimes (i (array-total-size array) retval)
    (setf (row-major-aref retval i)
          (funcall function (row-major-aref array i)))))

(defmacro zapa (f a c)
  `(let* ((tmp (array-map #'(lambda (x) (,f ,c x)) ,a)))
  (setq ,a tmp)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; cl-mf6.lisp ends here
