;;; example-0.lisp --- 
;; 
;; Filename: example-0.lisp
;; Description: 
;; Author: William Witterick
;; Maintainer: 
;; Created: Sat Mar 20 18:23:46 2021 (+0000)
;; Version: 
;; Last-Updated: 
;;           By: 
;;     Update #: 0
;; URL: 
;; Keywords: 
;; Compatibility: 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Commentary: 
;; 
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Change Log:
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Code:

(in-package #:cl-mf6)

(sb-posix:chdir "/home/william/src/modflow6-largetestmodels/test1002_biscqtg_dev")

(initialise)

(defvar *simulation-length* (- (get-end-time) (get-start-time)))
(defvar *mxiter* (get-value (get-var-address "MXITER" "SLN_1")))
(defvar *ptr-kh* (get-var-address  "K11" "BISCAYNE"  "NPF"))
(defvar *ptr-hd* (get-var-address  "X" "SLN_1"))
(defvar *kh*)
(defvar *hd*)

(setf *kh* (get-value *ptr-kh*))

(loop while (< (get-current-time) (get-end-time))
      do
	 (set-value *ptr-kh* (zapa * *kh* 1.01))

	 (prepare-time-step (get-time-step))

	 (loop for soln from 0 below (get-subcomponent-count)
	       do
		  (prepare-solve (+ 1 soln))
		  (loop for kiter from 1 below *mxiter*
			do
			   (if (do-solve)
			       (return)))
		  (finalise-solve (+ 1 soln)))

	 (finalise-time-step)

	 ;; (setf *hd* (get-value *ptr-hd*))

	 (format t "max kh: ~d time: ~d~%"
		 (reduce #'max *kh*)
		 (get-current-time)))

(finalise)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; example-0.lisp ends here
